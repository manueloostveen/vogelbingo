import BingoGrid from './components/BingoGrid';

export default function Home() {
  return (
    <div>
      <BingoGrid />
    </div>
  );
}
