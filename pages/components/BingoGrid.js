import React, { useState, useEffect } from 'react';
import BingoTile from './BingoTile';
import styles from './BingoGrid.module.css';
import { shuffle } from 'lodash';

const birdNames = ["Appelvink", "Boomklever", "Boomkruiper", "Boomleeuwerik", "Buizerd", "Ekster", "Gaai", "Geelgors", "Gekraagde roodstaart", "Glanskop", "Goudhaan", "Goudvink", "Grasmus", "Grote bonte specht", "Grote kruisbek", "Grote lijster", "Havik", "Heggenmus", "Holenduif", "Houtduif", "Houtsnip", "Keep", "Kleine barmsijs", "Kleine bonte specht", "Kleine vliegenvanger", "Kneu", "Koolmees", "Koperwiek", "Korhoen", "Kramsvogel", "Kruisbek", "Kuifmees", "Matkop", "Merel", "Middelste bonte specht", "Nachtegaal", "Nachtzwaluw", "Nijlgans", "Noordse nachtegaal", "Notenkraker", "Oehoe", "Orpheusspotvogel", "Ortolaan", "Pallas boszanger", "Pestvogel", "Pimpelmees", "Putter", "Raaf", "Ransuil", "Ringmus", "Rode wouw", "Roek", "Roodborst", "Scharrelaar", "Siberische boompieper", "Sijs", "Sperwer", "Spotvogel", "Spreeuw", "Staartmees", "Steenarend", "Tjiftjaf", "Tuinfluiter", "Vink", "Vuurgoudhaan", "Wespendief", "Wielewaal", "Winterkoning", "Zanglijster", "Zomertortel", "Zwarte kraai", "Zwarte mees", "Zwarte ooievaar", "Zwarte specht", "Zwarte wouw", "Zwartkop"];

export default function BingoGrid() {
  const [clickedTiles, setClickedTiles] = useState([]);
  const [shuffledTiles, setShuffledTiles] = useState([]);

  useEffect(() => {
    setShuffledTiles(shuffle(birdNames.slice()).slice(0, 25));
  }, []);

  const handleTileClick = (name) => {
    if (clickedTiles.includes(name)) {
      setClickedTiles(clickedTiles.filter((tile) => tile !== name));
    } else {
      setClickedTiles([...clickedTiles, name]);
    }
    console.log(name)
  };
  

  const tiles = shuffledTiles.map((name, index) => {
    if (index === 12) {
      return (
        <BingoTile key={name} text="*" disabled={true} onClick={() => {}} />
      );
    } else {
      return (
        <BingoTile
          key={name}
          text={name}
          isClicked={clickedTiles.includes(name)}
          onClick={() => handleTileClick(name)}
        />
      );
    }
  });

  return (
    <div className={styles.container}>
      <div className={styles.grid}>{tiles}</div>
    </div>
  );
}
