import React, { useState } from 'react';

export default function BingoTile({ text, isClicked, onClick, disabled }) {
//   const [isClicked, setIsClicked] = useState(false);

  const handleClick = () => {
    onClick();
  };

  const style = {
    opacity: isClicked || disabled ? 0.5 : 1,
    width: '100px',
    height: '100px',
    borderRadius: '0',
  };

  return (
    <button style={style} onClick={handleClick}>
      {text}
    </button>
  );
}
